#include <iostream>
#include <string.h>
#include <ctime>
#include <windows.h>

using namespace std;


int main()

{ 

	string TITLEDVD[] = {

	"0",

	"Hachi: A Dog's Tale (2009) ",

	"The Curious Case of Benjamin Button (2008) ",

	"The Dark Knight (2008)",

	"A Star Is Born(2018) ",

	"Harry Potter and the Prisoner of Azkaban (2004)",

	"Toy Story 3 (2010) ",

	"Hairspray (2007)",

	"Aquaman (2018)",

	"The Hundred-Foot Journey (2014)",

	"Harry Potter and the Half-Blood Prince (2009)",

	"Forrest Gump (1994)",

	"The Notebook (2004)",

	"The Hunger Games (2012)",

	"The Matrix (1999)",
	
	"The Princess Diaries (2001)",

	"Harry Potter and the Order of the Phoenix  (2007)",

	"Twilight (2008)",

	"500 Days of Summer (2009)",

	"A walk to remember (2002)",

	"The Perks of being a Wallflower (2012)",

	"The Prestige (2006)",

	"Avatar (2009)",

	"Monster, Inc. (2001)",

	"Night at the Museum (2006)",

	"50 first date  (2004)"};


	string DESCRIPTION[] = {

	"0",

	"A Dog's Tale is based on a true story of the love and devotion between a man and a dog.",

	"Tells the story of Benjamin Button, a man who starts aging backwards with bizarre consequences.",

	"When the menace known as The Joker emerges from his mysterious past, he wreaks havoc and chaos on the people of Gotham. The Dark Knight must accept one of the greatest psychological and physical tests of his ability to fight injustice.",

	"A musician helps a young singer find fame as age and alcoholism send his own career into a downward spiral.",

	"It's Harry's third year at Hogwarts; not only does he have a new Defense Against the Dark Arts teacher, but there is also trouble brewing. Convicted murderer Sirius Black has escaped the Wizards' Prison and is coming after Harry.",

	"The toys are mistakenly delivered to a day-care center instead of the attic right before Andy leaves for college, and it's up to Woody to convince the other toys that they weren't abandoned and to return home.",

	"Pleasantly plump teenager Tracy Turnblad teaches 1962 Baltimore a thing or two about integration after landing a spot on a local TV dance show.",

	" Arthur Curry, the human-born heir to the underwater kingdom of Atlantis, goes on a quest to prevent a war between the worlds of ocean and land.",

	"The Kadam family leaves India for France where they open a restaurant directly across the road from Madame Mallory's Michelin-starred eatery.",

	"As Harry Potter begins his sixth year at Hogwarts, he discovers an old book marked as 'the property of the Half-Blood Prince' and begins to learn more about Lord Voldemort's dark past.",

	"The presidencies of Kennedy and Johnson, the events of Vietnam, Watergate, and other history unfold through the perspective of an Alabama man with an IQ of 75.",

	"A poor yet passionate young man falls in love with a rich young woman, giving her a sense of freedom, but they are soon separated because of their social differences.",

	"Katniss Everdeen voluntarily takes her younger sister's place in the Hunger Games: a televised competition in which two teenagers from each of the twelve Districts of Panem are chosen at random to fight to the death.",

	"A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.",

	"Mia Thermopolis has just found out that she is the heir apparent to the throne of Genovia. With her friends Lilly and Michael Moscovitz in tow, she tries to navigate through the rest of her sixteenth year.",

	"With their warning about Lord Voldemort's return scoffed at, Harry and Dumbledore are targeted by the Wizard authorities as an authoritarian bureaucrat slowly seizes power at Hogwarts.",

	"When Bella Swan moves to a small town in the Pacific Northwest to live with her father, she meets the reclusive Edward Cullen, a mysterious classmate who reveals himself to be a 108-year-old vampire. Despite Edward's repeated cautions, Bella can't help but fall in love with him, a fatal move that endangers her own life when a coven of bloodsuckers try to challenge the Cullen clan.",

	"An offbeat romantic comedy about a woman who doesn't believe true love exists, and the young man who falls for her.",

	"The story of two North Carolina teens, Landon Carter and Jamie Sullivan, who are thrown together after Landon gets into trouble and is made to do community service.",

	" An introvert freshman is taken under the wings of two seniors who welcome him to the real world. ",

	"After a tragic accident, two stage magicians engage in a battle to create the ultimate illusion while sacrificing everything they have to outwit each other.",

	"A paraplegic Marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",

	"In order to power the city, monsters have to scare children so that they scream. However, the children are toxic to the monsters, and after a child gets through, 2 monsters realize things may not be what they think.",

	"A newly recruited night security guard at the Museum of Natural History discovers that an ancient curse causes the animals and exhibits on display to come to life and wreak havoc.",

	"Henry Roth is a man afraid of commitment up until he meets the beautiful Lucy. They hit it off and Henry think he's finally found the girl of his dreams, until he discovers she has short-term memory loss and forgets him the next day.",};

	string actors[] = {

	"0",

	"Richard Gere, Joan Allen ",

	"Brad Pitt, Cate Blanchett, Taraji P. Henson",

	"Christian Bale, Heath Ledger, Aaron Eckhart, Michael Caine",

	"Bradley Cooper, Lady Gaga, Sam Elliot ",

	"Daniel Radcliffe, Rupert Grint, Emma Watson",

	"Tom Hanks, Tim Allen, Joan Cusack, Don Tickles", 

	"Nikki Blonsky, John Travolta, Michelle Pfeiffer, Amanda Byness",

	"Jason Momoa, Amber Heard, Willem Dafoe, Patrick Wilson",

	"Helen Mirren, Om Puri, Manish Dayal, Charlotte Le Bon",

	"Daniel Radcliffe, Rupert Grint, Emma Watson",

	"Tom Hanks, Robin Wright, Gary Sinise, Sally Field",

	"Ryan Gosling, Rachel McAdams, James Garner, Gena Rowlands, Sam Shepard, Joan Allen",

	"Jennifer Lawrence, Josh Hutcherson, Liam Hemsworth",

	"Keanu Reeves, Laurence Fishburne, Carrie-Anne Moss",

	"Anne Hathaway, Julie Andrews, Heather Matarazzo",

	"Daniel Radcliffe, Rupert Grint, Emma Watson",

	"Kristen Stewart, Robert Pattinson, Billy Burke, Taylor Lautner, ",

	" Joseph Gordon-Levitt, Zooey Deschanel",

	"Mandy Moore, Shane West ",

	" Logan Lerman, Emma Watson, Ezra Miller",

	"Christian Bale, Hugh Jackman, Scarlett Johansson, Michael Caine",

	"Sam Worthington, Zoe Saldana, Stephen Lang, Michelle Rodriguez",

	"John Goodman, Billy Crystal, Steve Buscemi, James Coburn, Jennifer Tilly",

	"Ben Stiller, Robin Williams, Owen Wilson, Steve Coogan",

	"Adam Sandler, Drew Barrymore "};

	string director[] = {

	"0",

	"Lasse Hallstrom ",

	"David Fincher",

	"Christopher Nolan",

	"Bradley Cooper ",

	"Alfonso Cuaron",

	"Lee Unkrich",

	"Adam Shankman ",

	"James Wan",

	"Lasse Hallstrom",

	"David Yates ",

	"Robert Zemeckis ",

	"Nick Cassavetes ",

	"Gary Ross",

	"Lana Wachowski, Lilly Wachowski",

	"Garry Marshall" ,

	"David Yates ",

	"Catherine Hardwicke ",

	"Marc Webb ",

	"Adam Shankman ",

	"Stephen Chbosky ",

	"Christopher Nolan ",

	"James Cameron",

	"Pete Docter ",

	"Shawn Levy",

	"Peter Segal"};

string producers[] = {

	"0",

	"Richard Gere",

	"Cean Chaffin, Kathleen Kennedy, Frank Marshall",

	"Christopher Nolan, charles Roven, Emma Thomas",

	"Bill Gerber, Jon Peters ",

	"Chris Columbus, David Heyman, Mark Radcliffe",

	"Darla Anderson ",

	"Craig Zadan, Neil Meron",

	"Peter Safran, Rob Cowan",

	"Juliet Blake, Steven Spielberg, Oprah Winfrey",

	"David Heyman, David Barron ",

	"Wendy Finerman, Steve Tisch, Steve Starkey",

	"Lynn Harris, Mark Johnson",

	"Nina Jacobson",

	"Keanu Reeves, Wachowski Brothers",

	"Whitney Houston, Debra Martin Chase",

	"David Heyman, David Barron",

	"Wyck Godfrey, Mark Morgan",

	"Mason Novick",

	"Denise Di Novi, Hunt Lowry ",

	"Lianne Halfon, Russell Smith, John Malkovich",

	"Christopher Nolan, Emma Thomas, Aaron Ryder",

	"James Cameron, Jon Landau",

	"Darla Anderson",

	"Shawn Levy Chris Columbus" ,

	"Jack Giarraputo, Steve Golin, Nancy Juvonen"};

	string dvdstatus[] = {"0","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  "};

	string customers[41];

	string dvdrented[41];

	string daterented[41];

	string customername;

	int selecteddvd;

	bool x2 = true;

	int optionchoice;

	int dvdoptionchoice;

	int xc3=0;

	while(x2 = true)

	{

		system("CLS");

		//thedvd();                                                             

		cout<< "WELCOME!" << endl;

	

			cout << "\nOPTIONS:" << endl 

			<< "1 : DVD List" << endl

			<< "2 : View a dvd" << endl

			<< "3 : Rent/Check-out DVD" << endl 

			<< "4 : Return DVD" << endl 

			<< "5 : exit" << endl
			
			<< "\n\nEnter the number of your chosen option: ";

			

		cin >> dvdoptionchoice;

		if(cin.fail())

		{ 
			cin.clear();

     		cin.ignore();

			system("CLS");

			cout << "INVALID INPUT" << endl;

			system("PAUSE");

			system("CLS");

		}

		if (dvdoptionchoice == 1)

		{ 

			system("CLS");

		//	thedvd();

			cout << "DVD LIST" << endl <<endl;

			cout << "STATUS:                #: TITLE:" << endl;

			for(int x1=1;x1<=25;x1++)

				{

					cout << dvdstatus[x1] << "            " << x1 << ": "<< TITLEDVD[x1] << endl;

				}

			cout << "END OF LIST" << endl << endl;

			system("PAUSE");

		}

		else if (dvdoptionchoice == 2)

		{	

			system("CLS");

		//	thedvd();

			cout << "DVD LIST"  << endl <<endl;

			cout << "STATUS:                TITLE:" << endl;

			for(int x1=1;x1<=25;x1++)

				{

					cout << dvdstatus[x1] << "            " << x1 << ": "<< TITLEDVD[x1] << endl;

				}

			cout << "END OF LIST" << endl << endl;

			cout << "View a dvd by seslcting its number: ";

			cin >> selecteddvd;

			if(cin.fail() or selecteddvd <= 0 or selecteddvd >=26)

			{
				cin.clear();

	     		cin.ignore();

				cout << "INVALID INPUT" << endl;

				system("PAUSE");

				system("CLS");

			}

			else

			{

				system("CLS");

			//	thedvd();

				cout << "GREAT FILMS" << endl;

				cout << "TITLE: " << TITLEDVD[selecteddvd] << endl 

					 << "STATUS: " << dvdstatus[selecteddvd] << endl

					 << "SYNOPSIS: " << DESCRIPTION[selecteddvd] << endl 

					 << "DIRECTOR: " << director[selecteddvd] << endl 

					 << "ACTORS: " << actors[selecteddvd] << endl

					 << "Producers: " << producers[selecteddvd] << endl 

					 << endl;

				system("PAUSE");

			}

		}

		else if (dvdoptionchoice == 3)

		{

			system("CLS");

		//	thedvd();
			
			cout << "DVD LIST"  << endl <<endl;

			cout << "STATUS:                TITLE:" << endl;

			for(int x1=1;x1<=25;x1++)

				{

					cout << dvdstatus[x1] << "            " << x1 << ": "<< TITLEDVD[x1] << endl;

				}

			cout << "END OF LIST" << endl << endl;

					///////////NUMBER

					bool r = true;

					while(r == true)

					{

						cout << "To rent a dvd select its number: ";

						cin >> selecteddvd;

						if(cin.fail() or selecteddvd <= 0 or selecteddvd >=26)

						{

							cout << "INVALID INPUT" << endl;

							cin.clear();

							cin.ignore();

							r = true;

						}

						else

						{

							r = false;

						}

					}

					if (dvdstatus[selecteddvd] == "UNAVAILABLE")

					{

						cout << "THE DVD YOU CHOSE IS UNAVAILABLE" << endl;

						system("PAUSE");

					}

					else

					{

						bool p = true;

						while(p == true)

						{

							cout << "Enter customer's Name(Separated by '_'): ";

							cin >> customers[xc3];

							if(cin.fail())

							{

								cin.clear();

								p = true;

							}

							else

							{

								p = false;

							}

						}

						dvdrented[xc3] = TITLEDVD[selecteddvd];


						time_t curr_time;

						tm * curr_tm;

						char date_string[100];

						time(&curr_time);

						curr_tm = localtime(&curr_time);

						strftime(date_string, 50, "%B %d, %Y", curr_tm);

						daterented[xc3] = date_string;

						cout << "RECEIPT" << endl;

						cout << "CUSTOMER NAME: " << customers[xc3] << endl;

						cout << "DVD RENTED: " << dvdrented[xc3] << endl;

						cout << "DATE RENTED: " << daterented[xc3] << endl;

						system("PAUSE");

						xc3=++xc3;

						dvdstatus[selecteddvd] = "UNAVAILABLE";

					}

		}

		else if (dvdoptionchoice == 4)

		{


			system("CLS");

			cout << "RETURN_IN" << endl <<endl;

			cout << "STATUS:                #: TITLE:" << endl;

			for(int x1=1;x1<=25;x1++)

				{

					cout << dvdstatus[x1] << "            " << x1 << ": "<< TITLEDVD[x1] << endl;

				}

			cout << "END OF LIST" << endl << endl;

					///////////NUMBER

					bool r = true;

					while(r == true)

					{

						cout << "To return a DVD chose its number: ";

						cin >> selecteddvd;

						if(cin.fail() or selecteddvd <= 0 or selecteddvd >=26)

						{

							

							cout << "INVALID INPUT" << endl;

							cin.clear();

							cin.ignore();

							r = true;

						}

						else

						{

							r = false;

						}

					}

					if (dvdstatus[selecteddvd] == "AVAILABLE  ")

					{

						cout << "THE DVD YOU CHOSE IS AVAILABLE U CAN NOT RETURN IT" << endl;

						system("PAUSE");

					}

					else

					{
						dvdstatus[selecteddvd] = "AVAILABLE  ";

						cout << "The DVD you borrowed is successfully returned" << endl;

						system("PAUSE");

					}

			

		}

		else if (dvdoptionchoice == 5)

		{

		//	thedvd();
			exit(0);

		}

		else

		{

			cout << "INVALID INPUT" << endl;

			system("PAUSE");

		}

	}

}